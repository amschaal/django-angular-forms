var angular_forms = angular.module('angular-forms', []);
 
angular_forms.service('AngularFormService',['$http', function($http) {
	var errors = {};
	this.getErrors = function(name,prefix){
		if (!prefix)
			return errors[name] ? errors[name] : [];
		if (!errors[prefix])
			return [];
		return errors[prefix][name] ? errors[prefix][name] : []; 
	};
	this.post = function(url,data,params){
		console.log(params);
		var params = params ? params : {};
		$http.post(
			url,
			data
		).success(function(data, status, headers, config) {
			if (data.errors){
				if (params.prefix)
					errors[prefix]=data.errors;
				else
					errors=data.errors;
				if (params.onError)
					params.onError(data,status);
			}
			else{
				if (params.prefix)
					errors[prefix] = [];
				else
					errors = [];
				if (params.onSuccess)
					params.onSuccess(data,status);
			}
		});
	}
}
]);
		

angular_forms.directive('angularAjaxForm',  ['AngularFormService',function(AngularFormService) {
		console.log('directive');
	  return {
	    restrict: 'EA',
	    /* transclude and link function necessary to ensure that the isolated scope is used by the enclosed markup */
	    transclude: true,
	    link: function(scope, element, attrs, ctrl, transclude) {
	        transclude(scope, function(clone) {
	         element.append(clone);
	        });
	      },
	    scope: {url: '@', prefix: '@',onSuccess:'=',onError:'='},
	    controller: function($scope, $http, $element){
//	    	console.log('controller!',$scope.prefix, $scope[$scope.prefix]);
	    	
	    	this.$scope = $scope;
	    	$scope.getErrors = function(name){
	    		return AngularFormService.getErrors(name);
	    	};
	    	$scope.submit = function(data){
	    		console.log($scope.prefix);
	    		AngularFormService.post($scope.url,$scope[$scope.prefix],{'onSuccess':$scope.onSuccess,'onError':$scope.onError});
	    	}
	  }
	}
	}
]);
