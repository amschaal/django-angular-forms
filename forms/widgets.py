from django import forms
from django.utils.safestring import mark_safe

class AngularSelectWidget(forms.TextInput):
    def render(self, name, value, attrs=None):
        print name
        print value
        print attrs
        print self.attrs
        attrs.update({'class':'hidden__','ng-value':self.attrs['ng-model'],'ng-model':self.attrs['ng-model']})
        html = super(AngularSelectWidget,self).render(name,value,attrs)
        html = """<h1>{[%(ng-model)s]}</h1>
<ui-select ng-model="%(ng-model)s"
          theme="bootstrap"
          ng-disabled="disabled"
          reset-search-input="false"
          on-select="selectChoice($item, $model)"
          style="width: 300px;">
 <ui-select-match placeholder="Search...">{[$select.selected.%(field)s]}</ui-select-match>
 <ui-select-choices repeat="choice in choices track by $index"
          refresh="refreshChoices($select.search)"
          refresh-delay="0">
   <div ng-bind-html="choice.%(field)s | highlight: $select.search"></div>
 </ui-select-choices>
 </ui-select>"""%self.attrs+html
        return mark_safe(html)