from django.views.generic.base import View
from django.http import JsonResponse

class FormMixin(object):
    form_class = None
    lookup_field = 'pk'
    kwarg_field = 'pk'
    model = None
    def get_form_class(self,request,*args,**kwargs):
        return self.form_class
    def get_instance(self,request,*args,**kwargs):
        if not self.model:
            return None
        return self.model.objects.get(**{self.lookup_field:kwargs[self.kwarg_field]})
    

class FormView(FormMixin,View):
    """
    A view that renders a template.  This view will also pass into the context
    any keyword arguments passed by the url conf.
    """
    def post(self, request, *args, **kwargs):
#         context = self.get_context_data(**kwargs)
#         return self.render_to_response(context)
        import json
        form_class = self.get_form_class(request,*args,**kwargs)
        instance = self.get_instance(request,*args,**kwargs)
        if instance:
            form = form_class(json.loads(request.body),instance=instance)
        else:
            form = form_class(json.loads(request.body))
        if form.is_valid():
            form.save()
            return JsonResponse({'status':'ok'})
        else:
            return JsonResponse({'errors':form.errors})