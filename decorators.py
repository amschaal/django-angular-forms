from crispy_forms.helper import FormHelper
import json
from django.utils.safestring import mark_safe

def AngularFormDecorator(form_class):
    orig_init = form_class.__init__
    def __init__(self, *args, **kwargs):
        attrs = kwargs.pop('attrs',{})
        self.url = kwargs.pop('url','')
        orig_init(self, *args, **kwargs) # call the original __init__
        prefix = kwargs.pop('prefix', None)
        
        
        ng_init = '' #used for initializing ng-models to bound values
        for field in self.fields.keys():
            if prefix:
                ng_model = '%s.%s'%(prefix,field)
            else:
                ng_model = field
            self.fields[field].widget.attrs.update({'ng-model':ng_model})
            value = self.initial.get(field, self.fields[field].initial)
            if value:
                ng_init += '%s=%s;'%(ng_model,json.dumps(value))
            self.fields[field].widget.field_instance = self.fields[field]
            self.fields[field].widget.form_instance = self 
        self.helper = FormHelper(self)
        attrs.update({'ng-init':ng_init})
        self.helper.attrs.update(attrs)
#         angular-ajax-form url="{% url 'update_workflow' pk=workflow.id %}"
        self.helper.field_template = kwargs.get('field_template',"angular_forms/field.html")
        
    def start(self):
        return mark_safe('<div angular-ajax-form url="%s" prefix="%s">'% (self.url,self.prefix))
    form_class.start = start
    form_class.__init__ = __init__ # set the class' __init__ to the new one
    
    return form_class

# def AngularFormDecorator(form_class):
#     orig_init = form_class.__init__
#     def __init__(self, *args, **kwargs):
#         orig_init(self, *args, **kwargs) # call the original __init__
#         for field in self.fields.keys():
#             self.fields[field].widget.field_instance = self.fields[field] 
#     form_class.__init__ = __init__ # set the class' __init__ to the new one
#     
#     return form_class